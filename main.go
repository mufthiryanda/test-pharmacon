package main

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"pharmacon/routes"
	"time"
)

func main() {

	e := echo.New()
	routes.Api(e)

	s := http.Server{
		Addr:         ":8000",
		Handler:      e,
		ReadTimeout:  3 * time.Second,
		WriteTimeout: 3 * time.Second,
	}
	if err := s.ListenAndServe(); err != http.ErrServerClosed {
		panic(err)
	}

}
