package Attendance

import (
	"encoding/json"
	"github.com/boltdb/bolt"
)

type RepositoryParams struct {
	BoltDb *bolt.DB
}

func Repository(boltDb *bolt.DB) RepositoryInterface {
	return &RepositoryParams{
		BoltDb: boltDb,
	}
}

func (r RepositoryParams) RegisterAttendance(data User) error {

	db := r.BoltDb

	err := db.Update(func(tx *bolt.Tx) error {
		rootBucket, err := tx.CreateBucketIfNotExists([]byte("UserBucket"))
		if err != nil {
			return err
		}

		userBytes, err := json.Marshal(data)
		if err != nil {
			return err
		}

		err = rootBucket.Put([]byte(data.Id), userBytes)
		if err != nil {
			return err
		}

		return nil
	})

	return err

}

func (r RepositoryParams) GetUser() ([]User, error) {
	db := r.BoltDb
	var users []User

	err := db.View(func(tx *bolt.Tx) error {
		userBucket := tx.Bucket([]byte("UserBucket"))
		if userBucket == nil {
			return bolt.ErrBucketNotFound
		}

		return userBucket.ForEach(func(k, v []byte) error {
			var user User
			err := json.Unmarshal(v, &user)
			if err != nil {
				return err
			}
			users = append(users, user)
			return nil
		})
	})

	if err != nil {
		return nil, err
	}

	return users, nil
}

func (r RepositoryParams) FindUserByName(name string) ([]User, error) {

	db := r.BoltDb
	var users []User

	err := db.View(func(tx *bolt.Tx) error {
		userBucket := tx.Bucket([]byte("UserBucket"))
		if userBucket == nil {
			return bolt.ErrBucketNotFound
		}

		return userBucket.ForEach(func(k, v []byte) error {
			var user User
			err := json.Unmarshal(v, &user)
			if err != nil {
				return err
			}
			if user.Name == name {
				users = append(users, user)
			}
			return nil
		})
	})

	if err != nil {
		return nil, err
	}

	return users, nil
}

func (r RepositoryParams) CountUsersByName(name string) (int, error) {

	db := r.BoltDb

	count := 0

	err := db.View(func(tx *bolt.Tx) error {
		userBucket := tx.Bucket([]byte("UserBucket"))
		if userBucket == nil {
			return bolt.ErrBucketNotFound
		}

		return userBucket.ForEach(func(k, v []byte) error {
			var user User
			err := json.Unmarshal(v, &user)
			if err != nil {
				return err
			}
			if user.Name == name {
				count++
			}
			return nil
		})
	})

	if err != nil {
		return 0, err
	}

	return count, nil
}

func (r RepositoryParams) CountUsersByUserId(id string) (int, error) {
	db := r.BoltDb

	count := 0

	err := db.View(func(tx *bolt.Tx) error {
		userBucket := tx.Bucket([]byte("UserBucket"))
		if userBucket == nil {
			return bolt.ErrBucketNotFound
		}

		return userBucket.ForEach(func(k, v []byte) error {
			var user User
			err := json.Unmarshal(v, &user)
			if err != nil {
				return err
			}
			if user.Id == id {
				count++
			}
			return nil
		})
	})

	if err != nil {
		return 0, err
	}

	return count, nil
}
