package Attendance

import (
	"pharmacon/app/helper"
)

type ServiceInterface interface {
	RegisterAttendance() helper.HttpResponse
	AttendanceList() helper.HttpResponse
	ClockIn() helper.HttpResponse
	ClockOut() helper.HttpResponse
}

type RepositoryInterface interface {
	RegisterAttendance(data User) error
	GetUser() ([]User, error)
	CountUsersByName(name string) (int, error)
	FindUserByName(name string) ([]User, error)
	CountUsersByUserId(id string) (int, error)
}
