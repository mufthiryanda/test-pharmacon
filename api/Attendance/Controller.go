package Attendance

import "github.com/labstack/echo/v4"

func RegisterAttendanceController(c echo.Context) error {
	service := Service(c).RegisterAttendance()
	return c.JSON(service.Code, service)
}

func GetAttendanceController(c echo.Context) error {
	service := Service(c).AttendanceList()
	return c.JSON(service.Code, service)
}

func ClockInController(c echo.Context) error {
	service := Service(c).ClockIn()
	return c.JSON(service.Code, service)
}

func ClockOutController(c echo.Context) error {
	service := Service(c).ClockOut()
	return c.JSON(service.Code, service)
}
