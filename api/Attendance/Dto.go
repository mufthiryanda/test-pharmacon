package Attendance

import (
	"errors"
	"fmt"
	"github.com/boltdb/bolt"
	"github.com/go-playground/validator/v10"
	"github.com/golang-module/carbon/v2"
	"github.com/google/uuid"
)

type ClockInDto struct {
	UserId string
}

type CreateAttendanceDto struct {
	Id        string `json:"id"`
	Name      string `json:"name" validate:"required"`
	PhotoUrl  string `json:"photo_url" validate:"required"`
	CreatedAt string `json:"created_at"`
}

func (data CreateAttendanceDto) Validated(db *bolt.DB) (*CreateAttendanceDto, error) {

	validate := validator.New()

	err := validate.Struct(&data)

	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			errMsg := fmt.Sprintf("Error on field '%s' with value '%s' and tag '%s'", err.Field(), err.Value(), err.Tag())
			return nil, errors.New(errMsg)
		}
	}

	repo := Repository(db)
	countUser, err := repo.CountUsersByName(data.Name)

	if err != nil {
		if !errors.Is(err, bolt.ErrBucketNotFound) {
			return nil, errors.New(fmt.Sprintf("Error when count user by name: %s", err.Error()))
		}
	}

	if countUser > 0 {
		return nil, errors.New(fmt.Sprintf("User already exist"))
	}

	uid, err := uuid.NewUUID()
	if err != nil {
		return nil, errors.New(fmt.Sprintf("Error when generate uuid: %s", err.Error()))
	}

	newData := CreateAttendanceDto{
		Id:        uid.String(),
		Name:      data.Name,
		PhotoUrl:  data.PhotoUrl,
		CreatedAt: carbon.Now().ToIso8601String(),
	}

	return &newData, nil

}
