package Attendance

import (
	"fmt"
	"github.com/labstack/echo/v4"
	"image"
	"io"
	"log"
	"math"
	"mime/multipart"
	"os"
	"path/filepath"
	"pharmacon/app/database"
	"pharmacon/app/helper"
)

type ServiceParam struct {
	ctx echo.Context
}

func Service(ctx echo.Context) ServiceInterface {
	return &ServiceParam{ctx: ctx}
}

func (s ServiceParam) RegisterAttendance() helper.HttpResponse {

	ctx := s.ctx
	db := database.BoltDBConnection()
	defer func() {
		err := db.Close()
		if err != nil {
			panic(err)
		}
	}()

	photo, err := ctx.FormFile("user_photo")
	if err != nil {
		return helper.HttpResponseError(400, err.Error())
	}

	acceptedMimeTypes := []string{"image/jpeg", "image/png", "image/jpg"}
	mimeStatus := false
	for _, mimeType := range acceptedMimeTypes {
		if mimeType == photo.Header.Get("Content-Type") {
			mimeStatus = true
			break
		}
		continue
	}

	if !mimeStatus {
		return helper.HttpResponseError(400, "Photo mime type is not supported")
	}

	if photo.Size > 3*1024*1024 {
		return helper.HttpResponseError(400, "Photo size is too large, maximum 3MB")
	}

	cwd, err := os.Getwd()
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}
	storage := fmt.Sprintf("%s/storage/user_photo", cwd)

	if _, err := os.Stat(storage); os.IsNotExist(err) {
		err := os.Mkdir(storage, 0755)
		if err != nil {
			return helper.HttpResponseError(500, err.Error())
		}
	}

	data := CreateAttendanceDto{
		Name:     ctx.FormValue("name"),
		PhotoUrl: "-",
	}

	user, err := data.Validated(db)

	if err != nil {
		return helper.HttpResponseError(400, err.Error())
	}

	extension := filepath.Ext(photo.Filename)
	filename := fmt.Sprintf("%s%s", user.Id, extension)
	filePath := filepath.Join(storage, filename)

	src, err := photo.Open()
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}
	defer func(src multipart.File) {
		err := src.Close()
		if err != nil {
			panic(err)
		}
	}(src)

	dst, err := os.Create(filePath)
	defer func(dst *os.File) {
		err := dst.Close()
		if err != nil {
			panic(err)
		}
	}(dst)
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	_, err = io.Copy(dst, src)
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	repository := Repository(db)
	err = repository.RegisterAttendance(User{
		Id:        user.Id,
		Name:      user.Name,
		PhotoUrl:  filename,
		CreatedAt: user.CreatedAt,
	})

	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	return helper.HttpResponseSuccess(nil)

}

func (s ServiceParam) AttendanceList() helper.HttpResponse {

	db := database.BoltDBConnection()
	defer func() {
		err := db.Close()
		if err != nil {
			panic(err)
		}
	}()

	repository := Repository(db)
	users, err := repository.GetUser()

	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	if len(users) == 0 {
		return helper.HttpResponseError(404, "No data found")
	}

	return helper.HttpResponseSuccess(users)

}

func (s ServiceParam) ClockIn() helper.HttpResponse {

	cwd, err := os.Getwd()
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	db := database.BoltDBConnection()
	defer func() {
		err := db.Close()
		if err != nil {
			panic(err)
		}
	}()

	ctx := s.ctx

	userId := ctx.FormValue("user_id")
	if userId == "" {
		return helper.HttpResponseError(400, "User id is required")
	}

	repo := Repository(db)
	countUser, err := repo.CountUsersByUserId(userId)
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}
	if countUser == 0 {
		return helper.HttpResponseError(404, "User not found")
	}

	userPhoto := fmt.Sprintf("%s/storage/user_photo/%s.png", cwd, userId)
	if _, err := os.Stat(userPhoto); os.IsNotExist(err) {
		return helper.HttpResponseError(400, "User photo is required")
	}

	img1, err := loadImage(userPhoto)
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	imgFromUser, err := storeClockInOutImage(ctx, userId, "clock_in")
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	img2, err := loadImage(imgFromUser)
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	difference := calculateImageDifference(img1, img2)

	maxDifference := 441.67 * math.Sqrt(3)
	percentageMatch := (1.0 - (difference / maxDifference)) * 100.0

	if percentageMatch < 80 {
		return helper.HttpResponseError(400, "Failed ! Photo is not match")
	}

	return helper.HttpResponseFormatter(200, "Successfully Clock In", nil)
}

func (s ServiceParam) ClockOut() helper.HttpResponse {
	cwd, err := os.Getwd()
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	db := database.BoltDBConnection()
	defer func() {
		err := db.Close()
		if err != nil {
			panic(err)
		}
	}()

	ctx := s.ctx

	userId := ctx.FormValue("user_id")
	if userId == "" {
		return helper.HttpResponseError(400, "User id is required")
	}

	repo := Repository(db)
	countUser, err := repo.CountUsersByUserId(userId)
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}
	if countUser == 0 {
		return helper.HttpResponseError(404, "User not found")
	}

	userPhoto := fmt.Sprintf("%s/storage/user_photo/%s.png", cwd, userId)
	if _, err := os.Stat(userPhoto); os.IsNotExist(err) {
		return helper.HttpResponseError(400, "User photo is required")
	}

	img1, err := loadImage(userPhoto)
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	imgFromUser, err := storeClockInOutImage(ctx, userId, "clock_out")
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	img2, err := loadImage(imgFromUser)
	if err != nil {
		return helper.HttpResponseError(500, err.Error())
	}

	difference := calculateImageDifference(img1, img2)

	maxDifference := 441.67 * math.Sqrt(3)
	percentageMatch := (1.0 - (difference / maxDifference)) * 100.0

	if percentageMatch < 80 {
		return helper.HttpResponseError(400, "Failed ! Photo is not match")
	}

	return helper.HttpResponseFormatter(200, "Successfully Clock In", nil)
}

func loadImage(path string) (image.Image, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			panic(err)
		}
	}(file)

	img, _, err := image.Decode(file)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return img, nil
}

func storeClockInOutImage(ctx echo.Context, userId string, pathStorage string) (string, error) {

	photo, err := ctx.FormFile("user_photo")
	if err != nil {
		return "", err
	}

	acceptedMimeTypes := []string{"image/jpeg", "image/png", "image/jpg"}
	mimeStatus := false
	for _, mimeType := range acceptedMimeTypes {
		if mimeType == photo.Header.Get("Content-Type") {
			mimeStatus = true
			break
		}
		continue
	}

	if !mimeStatus {
		return "", err
	}

	if photo.Size > 3*1024*1024 {
		return "", err
	}

	cwd, err := os.Getwd()
	if err != nil {
		return "", err
	}
	storage := fmt.Sprintf("%s/storage/%s", pathStorage, cwd)

	if _, err := os.Stat(storage); os.IsNotExist(err) {
		err := os.Mkdir(storage, 0755)
		if err != nil {
			return "", err
		}
	}

	extension := filepath.Ext(photo.Filename)
	filename := fmt.Sprintf("%s%s", userId, extension)
	filePath := filepath.Join(storage, filename)

	src, err := photo.Open()
	if err != nil {
		return "", err
	}
	defer func(src multipart.File) {
		err := src.Close()
		if err != nil {
			panic(err)
		}
	}(src)

	dst, err := os.Create(filePath)
	defer func(dst *os.File) {
		err := dst.Close()
		if err != nil {
			panic(err)
		}
	}(dst)
	if err != nil {
		return "", err
	}

	_, err = io.Copy(dst, src)
	if err != nil {
		return "", err
	}

	return filePath, nil
}

func calculateImageDifference(img1, img2 image.Image) float64 {
	bounds1 := img1.Bounds()

	width := bounds1.Dx()
	height := bounds1.Dy()

	difference := 0.0

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			pixel1 := img1.At(x, y)
			pixel2 := img2.At(x, y)

			r1, g1, b1, _ := pixel1.RGBA()
			r2, g2, b2, _ := pixel2.RGBA()

			diff := math.Sqrt(math.Pow(float64(r1-r2), 2) + math.Pow(float64(g1-g2), 2) + math.Pow(float64(b1-b2), 2))
			difference += diff
		}
	}

	return difference / float64(width*height)
}
