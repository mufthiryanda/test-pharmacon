package Attendance

type User struct {
	Id        string `json:"id"`
	Name      string `json:"name"`
	PhotoUrl  string `json:"photo_url"`
	CreatedAt string `json:"created_at"`
}
