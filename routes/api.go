package routes

import (
	"github.com/labstack/echo/v4"
	"pharmacon/api/Attendance"
)

func Api(e *echo.Echo) {

	api := e.Group("/api")

	attendance := api.Group("/attendance")
	attendance.GET("/list", Attendance.GetAttendanceController)
	attendance.POST("/register", Attendance.RegisterAttendanceController)
	attendance.POST("/clock-in", Attendance.ClockInController)
	attendance.POST("/clock-out", Attendance.ClockOutController)

}
