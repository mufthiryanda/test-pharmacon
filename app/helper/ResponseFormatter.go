package helper

import "net/http"

type HttpResponse struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

func HttpResponseFormatter(code int, message string, data interface{}) HttpResponse {
	return HttpResponse{
		Code:    code,
		Message: message,
		Data:    data,
	}
}

func HttpResponseSuccess(data interface{}) HttpResponse {
	return HttpResponse{
		Code:    http.StatusOK,
		Message: "Successfully",
		Data:    data,
	}
}

func HttpResponseError(code int, message string) HttpResponse {
	return HttpResponse{
		Code:    code,
		Message: message,
		Data:    nil,
	}
}
