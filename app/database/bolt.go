package database

import "github.com/boltdb/bolt"

func BoltDBConnection() *bolt.DB {
	db, err := bolt.Open("my.db", 0600, nil)
	if err != nil {
		panic(err)
	}
	return db
}
